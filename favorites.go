package main

import (
	"github.com/TuDatTr/favorites_api/app"
)

func main() {
	app := &app.App{}
	app.Initialize()
	app.Run(":3000")
}
