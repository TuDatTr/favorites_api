package model

import (
	"crypto/sha512"
	"encoding/base64"
	"io/ioutil"
	"os"

	"github.com/h2non/filetype"
)

// Todo add error detection for image saving (i.e. file already exists, throw error if its not an image)
func (u *Bookmarks) SaveImage() {
	imagepath := base64ToFile(u.ImagePath)
	u.ImagePath = imagepath
}

func (u *Bookmarks) SetLabel(label string) {
	u.Label = label
}

func (u *Bookmarks) SetLink(link string) {
	u.Link = link
}

func (u *Bookmarks) SetImagePath(imagePath string) {
	u.ImagePath = imagePath
}

func (u *Bookmarks) SetBackgroundColor(bgColor string) {
	u.BackgroundColor = bgColor
}

func (u *Bookmarks) SetTags(tags []Tags) {
	u.Tags = tags
}

func base64ToFile(file string) string {
	hasher := sha512.New()
	hasher.Write([]byte(file))
	filename := "./assets/images" + base64.URLEncoding.EncodeToString(hasher.Sum(nil)) + ".jpg"

	dec, err := base64.StdEncoding.DecodeString(file)
	if err != nil {
		panic(err)
	}

	f, err := os.Create(filename)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	if _, err := f.Write(dec); err != nil {
		panic(err)
	}
	if err := f.Sync(); err != nil {
		panic(err)
	}

	buf, _ := ioutil.ReadFile(filename)
	if !filetype.IsImage(buf) {
		f.Close()
		os.Remove(filename)
		return ""
	}

	return filename
}
