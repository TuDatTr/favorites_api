package model

import (
	"github.com/TuDatTr/favorites_api/app/crypt"
)

func (u *Users) HashPassword() {
	u.Password = crypt.HashString(u.Password)
}
