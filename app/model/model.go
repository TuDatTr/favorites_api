package model

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

type Users struct {
	gorm.Model
	UserName   string `json:"username"`
	Password   string `json:"password"`
	UserInfoID uint
	UserInfo   UserInfo `json:"userinfo"`
}

type UserInfo struct {
	gorm.Model
	FirstName     string `json:"firstname"`
	LastName      string `json:"lastname"`
	EMailAdress   string `json:"mail"`
	Bookmarks     []Bookmarks
	SearchHistory []SearchHistories
	SettingsID    uint
	Settings      Settings
}

type Bookmarks struct {
	gorm.Model
	UserInfoID      uint
	Label           string `json:"label"`
	Link            string `json:"url"`
	ImagePath       string `json:"image"`
	BackgroundColor string `json:"bgcolor"`
	Tags            []Tags
}

type Tags struct {
	gorm.Model
	BookmarksID uint
	Tag         string `json:"tag"`
}

type SearchHistories struct {
	gorm.Model
	UserInfoID uint
	SearchTerm string `json:"sterm"`
}

type Settings struct {
	gorm.Model
	Settings string `json:"settings" gorm:"default:'{show_greeting:true,bgcolor:\"#6D7AB5\",pb_path:\"./assets/images/default/pb.jpg\"}'"`
}

func DBMigrate(db *gorm.DB) *gorm.DB {
	db.AutoMigrate(&Bookmarks{})
	db.AutoMigrate(&Settings{})
	db.AutoMigrate(&SearchHistories{})
	db.AutoMigrate(&Tags{})
	db.AutoMigrate(&UserInfo{})
	db.AutoMigrate(&Users{})
	return db
}
