package app

import (
	"log"
	"net/http"

	"github.com/TuDatTr/favorites_api/app/handler"
	"github.com/TuDatTr/favorites_api/app/model"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

type App struct {
	Router *mux.Router
	DB     *gorm.DB
}

func (a *App) Initialize() {
	db, err := gorm.Open("sqlite3", "favorites.db")
	if err != nil {
		panic("failed to connect database")
	}
	a.DB = model.DBMigrate(db)
	a.DB.LogMode(true)
	a.Router = mux.NewRouter()
	a.setRouters()
}

func (a *App) setRouters() {
	a.Post("/login", a.Login)
	a.Get("/bookmarks", a.GetBookmarks)
	a.Post("/bookmarks", a.CreateBookmark)
	a.Put("/bookmarks", a.EditBookmark)
	a.Delete("/bookmarks", a.DeleteBookmark)
	a.Get("/settings", a.GetSettings)
	a.Put("/settings", a.EditSettings)
	a.Post("/test/user", a.CreateTestUser)
}

func (a *App) Get(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("GET")
}

func (a *App) Post(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("POST")
}

func (a *App) Put(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("PUT")
}

func (a *App) Delete(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("DELETE")
}
func (a *App) Run(host string) {
	log.Fatal(http.ListenAndServe(host, a.Router))
}

func (a *App) Login(w http.ResponseWriter, r *http.Request) {
	handler.Login(a.DB, w, r)
}

func (a *App) GetBookmarks(w http.ResponseWriter, r *http.Request) {
	handler.GetBookmarks(a.DB, w, r)
}

func (a *App) CreateBookmark(w http.ResponseWriter, r *http.Request) {
	handler.CreateBookmark(a.DB, w, r)
}

func (a *App) EditBookmark(w http.ResponseWriter, r *http.Request) {
	handler.EditBookmark(a.DB, w, r)
}

func (a *App) DeleteBookmark(w http.ResponseWriter, r *http.Request) {
	handler.DeleteBookmark(a.DB, w, r)
}

func (a *App) GetSettings(w http.ResponseWriter, r *http.Request) {
	handler.GetSettings(a.DB, w, r)
}

func (a *App) EditSettings(w http.ResponseWriter, r *http.Request) {
	handler.EditSettings(a.DB, w, r)
}

func (a *App) CreateTestUser(w http.ResponseWriter, r *http.Request) {
	handler.CreateTestUser(a.DB, w, r)
}
