package handler

import (
	"encoding/json"
	"net/http"

	"github.com/TuDatTr/favorites_api/app/model"
	"github.com/jinzhu/gorm"
)

func GetBookmarks(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	if !ValidJWT(w, r) {
		respondError(w, http.StatusUnauthorized, "Authorization failed.")
		return
	}

	bookmarks := []model.Bookmarks{}

	uid := GetUserIDFromJWT(r.Header.Get("token"))
	if uid <= 0 {
		respondError(w, http.StatusBadRequest, "Authentication failed.")
		return
	}

	for _, bookmark := range bookmarks {
		if bookmark.UserInfoID != uint(uid) {
			respondError(w, http.StatusBadRequest, "Authentication failed.")
			return
		}
	}

	userbookmarks := getBookmarksFromUser(uint(uid), db)
	respondJSON(w, http.StatusCreated, userbookmarks)
}

func EditBookmark(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	if !ValidJWT(w, r) {
		respondError(w, http.StatusUnauthorized, "Authorization failed.")
		return
	}

	bookmark := model.Bookmarks{}
	decoder := json.NewDecoder(r.Body)

	if err := decoder.Decode(&bookmark); err != nil {
		respondError(w, http.StatusBadRequest, "Authentication failed.")
		return
	}
	defer r.Body.Close()

	uid := GetUserIDFromJWT(r.Header.Get("token"))

	if uid <= 0 {
		respondError(w, http.StatusBadRequest, "Authentication failed.")
		return
	}

	if bookmark.UserInfoID != uint(uid) {
		respondError(w, http.StatusBadRequest, "Authentication failed.")
		return
	}

	dbbookmark := getBookmarkByName(uint(uid), bookmark.Label, db)
	if dbbookmark == nil {
		respondError(w, http.StatusBadRequest, "no bookmark found.")
		return
	}

	dbbookmark.SetLabel(bookmark.Label)
	dbbookmark.SetLink(bookmark.Link)
	bookmark.SaveImage()
	dbbookmark.SetImagePath(bookmark.ImagePath)
	dbbookmark.SetBackgroundColor(bookmark.BackgroundColor)
	dbbookmark.SetTags(bookmark.Tags)

	if err := db.Save(&dbbookmark).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondJSON(w, http.StatusCreated, dbbookmark)
}

func CreateBookmark(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	if !ValidJWT(w, r) {
		respondError(w, http.StatusUnauthorized, "Authorization failed.")
		return
	}

	bookmark := model.Bookmarks{}
	decoder := json.NewDecoder(r.Body)

	if err := decoder.Decode(&bookmark); err != nil {
		respondError(w, http.StatusBadRequest, "Authentication failed.")
		return
	}
	defer r.Body.Close()

	uid := GetUserIDFromJWT(r.Header.Get("token"))
	if uid <= 0 {
		respondError(w, http.StatusBadRequest, "Authentication failed.")
		return
	}

	if bookmark.UserInfoID != uint(uid) {
		respondError(w, http.StatusBadRequest, "Authentication failed.")
		return
	}

	if dbbookmark := getBookmarkByName(uint(uid), bookmark.Label, db); dbbookmark != nil {
		respondError(w, http.StatusBadRequest, "Duplicate bookmark detected.")
		return
	}

	bookmark.SaveImage()

	if err := db.Save(&bookmark).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondJSON(w, http.StatusCreated, bookmark)
}

func DeleteBookmark(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	if !ValidJWT(w, r) {
		respondError(w, http.StatusUnauthorized, "Authorization failed.")
		return
	}

	bookmark := model.Bookmarks{}
	decoder := json.NewDecoder(r.Body)

	if err := decoder.Decode(&bookmark); err != nil {
		respondError(w, http.StatusBadRequest, "Authentication failed.")
		return
	}
	defer r.Body.Close()

	uid := GetUserIDFromJWT(r.Header.Get("token"))
	if uid <= 0 {
		respondError(w, http.StatusUnauthorized, "Authorization failed.")
		return
	}

	if bookmark.UserInfoID != uint(uid) {
		respondError(w, http.StatusBadRequest, "Authentication failed.")
		return
	}

	dbbookmark := getBookmarkByName(uint(uid), bookmark.Label, db)
	if dbbookmark == nil {
		respondError(w, http.StatusBadRequest, "Couldn't find Bookmark")
		return
	}

	db.Delete(&dbbookmark)
	respondJSON(w, http.StatusCreated, dbbookmark)
}

func getBookmarkByName(userid uint, name string, db *gorm.DB) *model.Bookmarks {
	bookmark := model.Bookmarks{}
	tags := []model.Tags{}

	if err := db.Where("user_info_id = ? AND label = ?", userid, name).Find(&bookmark).Error; err != nil {
		return nil
	}
	db.Model(&bookmark).Related(&tags)
	bookmark.SetTags(tags)

	return &bookmark
}

func getBookmarksFromUser(userid uint, db *gorm.DB) *[]model.Bookmarks {
	bookmarks := []model.Bookmarks{}

	if err := db.Where("user_info_id = ?", userid).Find(&bookmarks).Error; err != nil {
		return nil
	}

	for k, v := range bookmarks {
		tags := []model.Tags{}
		db.Model(&v).Related(&tags)
		bookmarks[k].SetTags(tags)
	}

	return &bookmarks
}
