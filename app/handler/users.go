package handler

import (
	"encoding/json"
	"net/http"

	"github.com/TuDatTr/favorites_api/app/model"
	"github.com/jinzhu/gorm"
)

func Login(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	if ValidJWT(w, r) {
		respondJSON(w, http.StatusOK, "Authorization successful")
		return
	}

	user := model.Users{}
	decoder := json.NewDecoder(r.Body)

	if err := decoder.Decode(&user); err != nil {
		respondError(w, http.StatusBadRequest, "Authentication failed.")
		return
	}
	defer r.Body.Close()

	dbuser := checkUserPassword(&user, db)

	if dbuser == nil {
		respondError(w, http.StatusUnauthorized, "Authentication failed.")
		return
	}

	respondJSON(w, http.StatusOK, "")
}

func CreateTestUser(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	user := model.Users{}
	settings := model.Settings{}

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&user); err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	if dbuser := getUserByName(user.UserName, db); dbuser != nil {
		respondError(w, http.StatusBadRequest, "User aleady exists.")
		return
	}

	db.Create(&settings)
	user.UserInfo.Settings = settings

	if err := db.Save(&user).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	AddNewJWTToHeader(user.ID, w)
	respondJSON(w, http.StatusCreated, user)
}

func getUserByID(id int, db *gorm.DB) *model.Users {
	user := model.Users{}

	if err := db.Where("id = ?", id).Find(&user).Error; err != nil {
		return nil
	}

	return &user
}

func getUserByName(username string, db *gorm.DB) *model.Users {
	user := model.Users{}

	if err := db.Where("user_name = ?", username).Find(&user).Error; err != nil {
		return nil
	}

	return &user
}

// Add error as return
func checkUserPassword(u *model.Users, db *gorm.DB) *model.Users {
	user := model.Users{}

	if err := db.Where("id = ? AND password = ?", u.ID, u.Password).Find(&user).Error; err != nil {
		return nil
	}

	return &user
}
