package handler

import (
	"fmt"
	"net/http"
	"time"

	"github.com/TuDatTr/favorites_api/app/crypt"
	"github.com/dgrijalva/jwt-go"
)

func AddNewJWTToHeader(userid uint, w http.ResponseWriter) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"sub": userid,
		"exp": time.Now().AddDate(0, 0, 7).Unix(),
		"iat": time.Now().Unix(),
		"nbf": time.Now().Unix(),
	})

	tokenString, err := token.SignedString([]byte(crypt.GetSecret()))
	if err != nil {
		return
	}
	w.Header().Set("token", tokenString)
}

func ValidJWT(w http.ResponseWriter, r *http.Request) bool {
	jwt_token := r.Header.Get("token")
	if jwt_token != "" {
		userid := GetUserIDFromJWT(jwt_token)
		if userid != -1 {
			AddNewJWTToHeader(uint(userid), w)
			return true
		}
	}
	return false
}

func GetUserIDFromJWT(givenToken string) int {
	token, _ := parseJWT(givenToken)

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		uid := int(claims["sub"].(float64))
		return uid
	} else {
		return -1
	}
}

func parseJWT(givenToken string) (*jwt.Token, error) {
	token, err := jwt.Parse(givenToken, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(crypt.GetSecret()), nil
	})

	return token, err
}
