package handler

import (
	"encoding/json"
	"net/http"

	"github.com/TuDatTr/favorites_api/app/model"
	"github.com/jinzhu/gorm"
)

func GetSettings(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	if !ValidJWT(w, r) {
		respondError(w, http.StatusUnauthorized, "Authorization failed.")
		return
	}

	settings := getSettings(db, w, r)

	respondJSON(w, http.StatusCreated, &settings)
}

func EditSettings(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	if !ValidJWT(w, r) {
		respondError(w, http.StatusUnauthorized, "Authorization failed.")
		return
	}

	settings := model.Settings{}
	dbsettings := getSettings(db, w, r)
	decoder := json.NewDecoder(r.Body)

	if err := decoder.Decode(&settings); err != nil {
		respondError(w, http.StatusBadRequest, "Authentication failed.")
		return
	}
	defer r.Body.Close()

	dbsettings.SetSettings(settings.Settings)
	if err := db.Save(&dbsettings).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusCreated, &dbsettings)
}

func getSettings(db *gorm.DB, w http.ResponseWriter, r *http.Request) *model.Settings {
	uid := GetUserIDFromJWT(r.Header.Get("token"))
	if uid <= 0 {
		respondError(w, http.StatusUnauthorized, "Authorization failed.")
		return nil
	}

	return getSettingsFromUser(uint(uid), db)

}

func getSettingsFromUser(uid uint, db *gorm.DB) *model.Settings {
	userinfo := model.UserInfo{}
	settings := model.Settings{}

	if err := db.Where("id = ?", uid).Find(&userinfo).Error; err != nil {
		return nil
	}
	db.Model(&userinfo).Related(&settings)

	return &settings
}
