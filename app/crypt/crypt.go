package crypt

import (
	"io/ioutil"
	"log"
	"math/rand"
	"os"

	"golang.org/x/crypto/bcrypt"
)

const alphabet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~" // From `python -c "import string; print(string.printable)" `

func ComparePasswords(hashedPwd string, plainPwd string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hashedPwd), []byte(plainPwd))
	if err != nil {
		log.Println(err)
		return false
	}
	return true
}

func HashString(text string) string {
	hash, err := bcrypt.GenerateFromPassword([]byte(text), bcrypt.MinCost)
	if err != nil {
		log.Println(err)
		return ""
	}
	return string(hash)
}

func GetSecret() string {
	return getSecret()
}

func getSecret() string {
	secret := randomString(20)
	secretFile := "./secret"
	if _, err := os.Stat(secretFile); err == nil {
		b, err := ioutil.ReadFile(secretFile)
		if err != nil {
			log.Fatal(err)
		}
		secret = string(b)
	} else if os.IsNotExist(err) {
		err = ioutil.WriteFile(secretFile, []byte(secret), 0644)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		// Other error occured
		log.Fatal(err)
	}
	return secret
}

func randomString(length int) string {
	rndStr := make([]byte, length)
	for i := range rndStr {
		rndStr[i] = alphabet[rand.Intn(len(alphabet))]
	}
	return string(rndStr)
}
