module github.com/TuDatTr/favorites_api

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/google/go-cmp v0.3.1 // indirect
	github.com/gorilla/mux v1.7.3
	github.com/h2non/filetype v1.0.10
	github.com/jinzhu/gorm v1.9.11
	golang.org/x/crypto v0.0.0-20191202143827-86a70503ff7e
)
