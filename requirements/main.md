% Requirements: favorites\_api
% [https://www.gitlab.com/TuDatTr](https://www.gitlab.com/TuDatTr)
% 2020-02-02

# Outgoing

HTTP Method | URL | Explaination | Done
----------- | ---- | ------------ | ----
POST | `/login` | User sends username and password and receives a login token | [x]
POST | `/bookmarks`  | User sends a bookmark and receives the bookmark back | [x]
PUT | `/bookmarks`  | User sends a bookmark and receives the bookmark back | [x]
DELETE | `/bookmarks`  | User sends a bookmark and receives the bookmark back | [x]
GET | `/bookmarks`  | User gets his bookmarks | [x]
GET | `/settings` | User gets his settings | [x]
PUT | `/settings` | User sends settings and receives settings back | [x]
`*` | `/test/*` | Everything I use for testing | [ ]

## login

### login: POST

The user sends his credentials to receive a token. His credentials consist of
his username and his password. The password is hashed and the combination of the
username and password is compared to the database. If the authentication is
successfull the user receives a token. If it isn't he receives an error.
The token is a jwt. If the user already has a jwt and tries to login he
receives an error.

## bookmarks

### bookmarks: POST

The user sends a bookmark along with his token. The token is then checked
whether it is valid or not. If the token is valid the bookmark is saved in the
database and the token is refreshed. If the token is not valid the user receives
an error.

### bookmarks: PUT

The user sends a bookmark along with his token. The token is then checked
whether it is valid or not. If the token is valid and the bookmark already exists
in the database the entry is edited, if its not the user receives an error.
Regardless of this, the user receives a refreshed token.

### bookmarks: GET

The user sends a bookmark along with his token. The token is then checked
whether it is valid or not. If the token is valid the user receives all of his
bookmarks, filtered by `<value>` along with a refreshed token. If its not
valid the user receives an error.

### bookmarks: DELETE

The user sends a bookmark along with his token. The token is then checked
whether it is valid or not. If the token is valid the given bookmark is
deleted from the database along with his tags. If the token is not valid the
user receives an error.

## settings

### settings: GET

The user sends his token. The token is then checked whether it is valid or not.
If its valid the user receives his settings. If it's not valid the user
receives an error.

### settings: PUT

The user sends his settings and his token. The token is then verified. If the
token is valid the users settings are overwritten with the settings the user
sent. If the token is not valid the user receives an error.

## test

Every function under this path is used for testing purposes only.
